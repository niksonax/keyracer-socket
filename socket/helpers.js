import { texts } from "../data";

const chooseRandomText = (room) => {
  console.log(texts);
  const textIndex = Math.floor(Math.random() * texts.length);
  const text = texts[textIndex];
  room.text = text;
  return text;
};

export { chooseRandomText };
