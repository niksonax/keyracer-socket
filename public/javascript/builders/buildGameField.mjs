export const buildGameField = (isReady) => {
  const gameField = document.createElement("div");
  gameField.classList.add("game-container");
  gameField.innerHTML = `<button id="ready-btn">${
    isReady ? "Not Ready" : "Ready"
  }</button>`;
  return gameField;
};
