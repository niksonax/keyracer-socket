export const buildRoomCard = (name, online) => {
  const roomCard = document.createElement("div");
  roomCard.classList.add("room");
  roomCard.id = name;
  roomCard.innerHTML = `
                <p class="connected-users">${online} users connected</p>
                <h3 class="name">${name}</h3>
                <button class="join-btn" data-id=${name}>Join</button>
  `;
  return roomCard;
};
