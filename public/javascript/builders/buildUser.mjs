const username = sessionStorage.getItem("username");

export const buildUser = (name, isReady = false, progress = 0) => {
  const user = document.createElement("div");
  user.classList.add("user", name);
  user.innerHTML = `<div class="user-info">
                      <span class="ready-status-${
                        isReady ? "green" : "red"
                      }"></span>
                      <p class="user-name">${
                        username === name ? name + " (you)" : name
                      }</p>
                    </div>
                    <div class="user-progress">
                      <div class="user-progress-fill ${name}" style="width:${progress}%"></div>
                    </div>`;
  return user;
};
